tup = (4,5)
x,y= tup
print(x,y)

data = ['ACME',50,91.1,(2012,12,21)]
name,shares,price,(year, mon, date)=data
print(name, '\t',shares, '\t', price, '\t', year, '\t', mon, '\t', date)

why, shares, price, why = data # using a throwaway variable to store unnecessary data
print(why,shares,price,why)