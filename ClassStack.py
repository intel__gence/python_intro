class Stack:
    def __init__(self):
        self.arr = []

    def isEmpty(self):
        return self.arr==[]

    def push(self, x):
        self.arr.append(x)

    def pop(self):
        return self.arr.pop()
    

s=Stack()
print(s.isEmpty())
s.push(4)
s.push('dog')
print(s.pop())