nums = [1, 8, 2, 23, 7, -4, 18, 23, 42, 37, 2]
import heapq
heap = list(nums)
heapq.heapify(heap)
print(heap) # Prints the heap with heap[0] beaing the smallest item in the heap

# Subsequent items can be found by using the heapop() function
# which pops the first item and replaces it with the next smallest item

print(heapq.heappop(heap))
print(heap)

# The nsmallest()/nlargest() funcn. are used most appropriately if a relatively small number of items is to be found.
# If only the smallest/largest item is to be found, then using min()/max() funcn is more efficient.
print(min(heap))
print(max(heap))

# If N is about the same size as of the collection itself
# it is faster to sort it first and then take a slice of it,
# eg., sorted(items)[:N] or sorted(items)[-N:]
print(sorted(heap)[:6])
print(sorted(heap)[-6:])