# Build and return a list
def firstn(n):
    num, nums = 0, [ ]
    while num<n:
        nums.append(num)
        num += 1
    return nums

#sum_of_first_n = sum(firstn(1000000))

# Using generator function that yields items instead of returning a list
def first_n(n):
    num = 0
    while num<n:
        yield num
        num += 1

sum_of_first_n = sum(first_n(10))

print(sum_of_first_n) # Prints 499999500000

